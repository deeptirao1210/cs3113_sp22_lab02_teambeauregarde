#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <string.h>

double unifd(int min, int max);
long rand_vald(int seed);

int main(int argc, char* argv[])
{
    char* outfile_name;
    int numjobs;
    int seed;
    int min_arrival;
    int max_arrival;
    int min_runtime;
    int max_runtime;
    // option code adapted from https://azrael.digipen.edu/~mmead/www/Courses/CS180/getopt.html
    // which was in the powerpoint on thursday 7 april
    int c;

    if (argc != 15) {
        printf("Incorrect number of arguments provided, expected 10\n");
        exit(0);
    }

    while (1)
    {
        int option_index = 0;
        static struct option long_options[] =
        {
            {"workload-file", required_argument, NULL, 0},
            {"numjobs", required_argument, NULL, 0},
            {"seed", required_argument, NULL, 0},
            {"min-arrival", required_argument, NULL, 0},
            {"max-arrival", required_argument, NULL, 0},
            {"min-runtime", required_argument, NULL, 0},
            {"max-runtime", required_argument, NULL, 0},
            {"help", no_argument, NULL, 'h'},
            {NULL, 0, NULL, 0}
        };

        c = getopt_long(argc, argv, "-:", long_options, &option_index);
        if (c == -1)
            break;
        
        switch (c)
        {
            case 0:
                if (strcmp(long_options[option_index].name, "workload-file") == 0)
                    outfile_name = optarg;
                if (strcmp(long_options[option_index].name, "numjobs") == 0)
                    numjobs = (int) atoi(optarg);
                if (strcmp(long_options[option_index].name, "seed") == 0)
                    seed = (int) atoi(optarg);
                if (strcmp(long_options[option_index].name, "min-arrival") == 0)
                    min_arrival = (int) atoi(optarg);
                if (strcmp(long_options[option_index].name, "max-arrival") == 0)
                    max_arrival = (int) atoi(optarg);
                if (strcmp(long_options[option_index].name, "min-runtime") == 0)
                    min_runtime = (int) atoi(optarg);
                if (strcmp(long_options[option_index].name, "max-runtime") == 0)
                    max_runtime = (int) atoi(optarg);
                break;
            default:
                printf("?? getopt returned character code 0%o ??\n", c);
                exit(-1);
        }
    }

    FILE *outfile = fopen(outfile_name, "w");
    if (outfile == NULL)
    {
        printf("fatal: couldn't write to file %s\n", outfile_name);
        exit(-1);
    }

    int* arrival_times = malloc(sizeof(int) * numjobs);
    int* run_times = malloc(sizeof(int) * numjobs);

    // set the seed
    rand_vald(seed);
    for (int i = 0; i < numjobs; i++)
    {
        arrival_times[i] = unifd(min_arrival, max_arrival);
    }

    rand_vald(seed);
    for (int i = 0; i < numjobs; i++)
    {
        run_times[i] = unifd(min_runtime, max_runtime);
    }

    for (int i = 1; i <= numjobs; i++)
    {
        fprintf(outfile, "%i,%i,%i\n", i, arrival_times[i-1], run_times[i-1]);
    }

    fclose(outfile);
    free(arrival_times);
    free(run_times);
}

// these functions are from https://cse.usf.edu/~kchriste/tools/genunifd.c
// which is the website referenced in the project specification
double unifd(int min, int max)
{
    int    z;                     // Uniform random integer number
    int    unif_value;            // Computed uniform value to be returned
    // Pull a uniform random integer
    z = rand_vald(0);
    // Compute uniform discrete random variable using inversion method
    unif_value = z % (max - min + 1) + min;
    return(unif_value);
}

long rand_vald(int seed)
{
    const long  a =      16807;  // Multiplier
    const long  m = 2147483647;  // Modulus
    const long  q =     127773;  // m div a
    const long  r =       2836;  // m mod a
    static long x;               // Random int value
    long        x_div_q;         // x divided by q
    long        x_mod_q;         // x modulo q
    long        x_new;           // New x value

    // Set the seed if argument is non-zero and then return zero
    if (seed > 0)
    {
        x = seed;
        return(0.0);
    }

    // RNG using integer arithmetic
    x_div_q = x / q;
    x_mod_q = x % q;
    x_new = (a * x_mod_q) - (r * x_div_q);
    if (x_new > 0)
        x = x_new;
    else
        x = x_new + m;

    // Return a random integer number
    return(x);
}
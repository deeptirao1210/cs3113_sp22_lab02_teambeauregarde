#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <getopt.h>

//void sortArrivalInOrder(int arrival[],int runtime[], int n);
//void findResponseTime(int arrival[], int runtime[] ,int n,int response[]);
//void calculateTAT(int arrival[], int runtime[], int response[], int n, int tat[]);

// Declaring  single node of the queue
struct node
{
	int process_id;
	int arrival;
	int runtime;
	int tat;
	int response;
	struct node* next;
};

struct node * head = NULL;

//function declarations
struct node* addAtEnd(struct node* n);
struct node* calculateResponseTime(struct node* h);
struct node* findTAT(struct node* h);
void destroy(struct node* node);

void main(int argc, char* argv[])
{
	char input[30];
	int  num_of_processes = 0;
	char* token;

	//A 2-D array to record the  process Id, arrival time and runtime of the entries in the input file.
	int process[10000][3];

	char* input_file;
	char* output_file;

	int copt;

    if (argc != 5) {
        printf("Incorrect number of arguments provided, expected 5, got %i\n", argc);
        exit(0);
    }

    while (1)
    {
        int option_index = 0;
        static struct option long_options[] =
        {
            {"workload-file", required_argument, NULL, 0},
            {"result-file", required_argument, NULL, 0},
            {NULL, 0, NULL, 0}
        };

        copt = getopt_long(argc, argv, "-:", long_options, &option_index);
        if (copt == -1)
            break;
        
        switch (copt)
        {
            case 0:
                if (strcmp(long_options[option_index].name, "workload-file") == 0)
                    input_file = optarg;
                if (strcmp(long_options[option_index].name, "result-file") == 0)
                    output_file = optarg;
                break;
            default:
                printf("?? getopt returned character code 0%o ??\n", copt);
        }
    }

    FILE *fp = fopen(input_file, "r");
    if (!fp)
    {
        printf("fatal: couldn't read the file %s\n", input_file);
        exit(-1);
    }

    FILE *outfile = fopen(output_file, "w");
    if (!outfile)
    {
        printf("fatal: couldn't write to the file %s\n", output_file);
        fclose(fp);
        exit(-1);
    }

	while(fgets(input,30,fp) != NULL)
	{
		// tokenizing each entry in the input file on delimiter ','
		// entries are of the form: process_id,arrivaltime,runtime
		token =  strtok(input,",");
		int v =0;
		while(token !=NULL)
		{
			
			int temp1 = atoi(token);
			process[num_of_processes][v] = temp1;
			token = strtok(NULL,",");
			v+=1;
		}
		++num_of_processes;

	}

	
	fclose(fp);

	//creating a node for each entry of input file
	for(int i=0;i< num_of_processes;i++)
	{
		struct node* newNode = (struct node*)malloc(sizeof(struct node));
		newNode->process_id = i+1;
		newNode->arrival = process[i][1];
		newNode->runtime = process[i][2];
		newNode->tat = 0;
		newNode->response = 0;
		newNode->next = NULL;

		//linked list used as a queue by adding at the end and removing from the front...
		head = addAtEnd(newNode);

		
		
	}
	// FCFS: Response Time and TAT calculation
	head = calculateResponseTime(head);
	head = findTAT(head);
	
	//writing the queue contents to the stdout and into the output file
	struct node* ptr = head;
	ptr=ptr->next;
	while(ptr!=NULL)
	{
			
		printf("\n Process ID: %d,  Arrival Time : %d, Runtime: %d, Response Time: %d, TAT: %d ",ptr->process_id, ptr->arrival, ptr->runtime, ptr->response, ptr->tat);
		ptr=ptr->next;
	}
	
	struct node* p = head;
	p = p->next;
	for(int i=0; i< num_of_processes;i++)
	{
		fprintf(outfile,"%d,%d,%d,%d,%d\n",p->process_id,p->arrival,p->runtime,p->tat,p->response);
		p= p->next;
	}
	fclose(outfile);
	destroy(head);
}

struct node* addAtEnd(struct node* n)
{

	if(head == NULL)
	{
		head = (struct node*)malloc(sizeof(struct node));
		head->process_id = 0;
		head->arrival = 0;
		head->runtime = 0;
		head->response = 0;
		head->tat = 0;
		head->next = n;
		
	}
	else
	{
		struct node * p = head;
		while(p->next !=NULL)
		{
			p=p->next;
		}
		p->next = n;
	
	
	}
	return head;
}

struct node* calculateResponseTime(struct node* head)
{

	struct node* ptr = head;
	ptr=ptr->next;
	int temp=0;
	//ptr=ptr->next;	
	while(ptr !=NULL)
	{
		temp =temp + ptr->runtime;
		ptr=ptr->next;
		if(ptr!= NULL)
		{	
			ptr->response = temp- ptr->arrival;
		//	printf("\n %d ", ptr->response);
		}
	}	
	return head;
}

struct node* findTAT(struct node* h)
{
	struct node* p = h;
	while(p!=NULL)
	{
		p->tat = p->arrival + p->response + p->runtime;
		p = p->next;
	}

	return h;
}

void destroy(struct node* node)
{
	if (node == NULL)
		return;
	struct node* next = node->next;
	free(node);
	destroy(next);
}

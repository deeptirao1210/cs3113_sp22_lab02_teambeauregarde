#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <string.h>

#define INT_MAX 2147483647

typedef struct Node Node;

struct Node {
    int pid;
    int arrivalTime;
    int timeRun;
    int runtime;
    int turnaround;
    int response;
    int hasRun;
    Node* next;
};

void write(Node* head, FILE* outfile);
void addNode(Node* head, Node* toAdd);
void schedule(Node* head, Node* currentTask, int time);
Node* createNode(int pid, int arrivalTime, int runtime);
Node* getShortestJob(Node* head, int currentTime);
Node* _locateShortestJob(Node* node, Node* shortest, int currentTime);
int allTasksComplete(Node* head);
int areTasksAvailable(Node* head, int currentTime);
void destroy(Node* head);

Node* dummy;

int main(int argc, char* argv[])
{
    dummy = (Node*) malloc(sizeof(Node));
    dummy->arrivalTime = INT_MAX;
    dummy->runtime = INT_MAX;
    dummy->timeRun = 0;
    char* infile_name;
    char* outfile_name;
    // option code adapted from https://azrael.digipen.edu/~mmead/www/Courses/CS180/getopt.html
    // which was in the powerpoint on thursday 7 april
    int c;

    if (argc != 5) {
        printf("Incorrect number of arguments provided, expected 5, got %i\n", argc);
        free(dummy);
        exit(0);
    }

    while (1)
    {
        int option_index = 0;
        static struct option long_options[] =
        {
            {"workload-file", required_argument, NULL, 0},
            {"result-file", required_argument, NULL, 0},
            {NULL, 0, NULL, 0}
        };

        c = getopt_long(argc, argv, "-:", long_options, &option_index);
        if (c == -1)
            break;
        
        switch (c)
        {
            case 0:
                if (strcmp(long_options[option_index].name, "workload-file") == 0)
                    infile_name = optarg;
                if (strcmp(long_options[option_index].name, "result-file") == 0)
                    outfile_name = optarg;
                break;
            default:
                printf("?? getopt returned character code 0%o ??\n", c);
        }
    }

    FILE *infile = fopen(infile_name, "r");
    if (!infile)
    {
        printf("fatal: couldn't read the file %s\n", infile_name);
        free(dummy);
        exit(-1);
    }

    FILE *outfile = fopen(outfile_name, "w");
    if (!outfile)
    {
        printf("fatal: couldn't write to the file %s\n", outfile_name);
        fclose(infile);
        free(dummy);
        exit(-1);
    }

    // read in each line
    Node* head;
    int isFirst = 0;
    char line[100];
    while (fgets(line, 100, infile) != NULL)
    {
        char* pid = strtok(line, ",");
        char* arrivalTime = strtok(NULL, ",");
        char* runtime = strtok(NULL, ",");
        if (isFirst == 0)
        {
            isFirst = 1;
            head = createNode(atoi(pid), atoi(arrivalTime), atoi(runtime));
        }
        else
        {
            addNode(head, createNode(atoi(pid), atoi(arrivalTime), atoi(runtime)));
        }
    }

    schedule(head, NULL, 0);
    write(head, outfile);
    destroy(head);
    free(dummy);
    fclose(infile);
    fclose(outfile);
}

Node* createNode(int pid, int arrivalTime, int runtime)
{
    Node* node = malloc(sizeof(Node));
    node->pid = pid;
    node->arrivalTime = arrivalTime;
    node->runtime = runtime;
    node->turnaround = 0;
    node->response = -1;
    node->hasRun = 0;
    node->timeRun = 0;
    node->next = NULL;
    return node;
}

void addNode(Node* head, Node* toAdd)
{
    if (head->next == NULL)
    {
        head->next = toAdd;
        return;
    }
    addNode(head->next, toAdd);
}

Node* getShortestJob(Node* head, int currentTime)
{
    return _locateShortestJob(head, dummy, currentTime);
}

Node* _locateShortestJob(Node* node, Node* shortest, int currentTime)
{
    if (node == NULL)
        return shortest;
    if (node->hasRun == 1)
        return _locateShortestJob(node->next, shortest, currentTime);
    if (node->runtime - node->timeRun < shortest->runtime - shortest->timeRun && currentTime >= node->arrivalTime)
        shortest = node;
    return _locateShortestJob(node->next, shortest, currentTime);
}

int allTasksComplete(Node* head)
{
    if (head->hasRun == 0)
        return 0;
    if (head->next == NULL)
        return 1;
    return allTasksComplete(head->next);
}

void schedule(Node* head, Node* currentTask, int time)
{
    if (allTasksComplete(head) == 1)
        return;
    if (areTasksAvailable(head, time) == 0)
        return schedule(head, currentTask, time + 1);
    Node* shortestAvailable = getShortestJob(head, time);
    if (currentTask == NULL)
    {
        currentTask = shortestAvailable;
        currentTask->response = time;
    }
    // continue execution
    if (currentTask->pid == shortestAvailable->pid)
    {
        if (currentTask->runtime == currentTask->timeRun)
        {
            currentTask->hasRun = 1;
            currentTask->turnaround = time - currentTask->arrivalTime;
            currentTask->timeRun++;
            schedule(head, currentTask, time);
        } else {
            currentTask->timeRun++;
            schedule(head, currentTask, time + 1);
        }
    }
    else // switch tasks
    {
        if (areTasksAvailable(head, time) == 0)
        {
            schedule(head, shortestAvailable, time + 1);
        } else {
            shortestAvailable = getShortestJob(head, time);
            if (shortestAvailable->response == -1)
                shortestAvailable->response = time - shortestAvailable->arrivalTime;
            shortestAvailable->timeRun++;
            schedule(head, shortestAvailable, time + 1);
        }
    }
}

void write(Node* head, FILE* outfile)
{
    fprintf(outfile, "%i,%i,%i,%i,%i\n", head->pid, head->arrivalTime, head->runtime, head->turnaround, head->response);
    if (head->next == NULL)
        return;
    write(head->next, outfile);
}

void destroy(Node* head)
{
    if (head == NULL)
        return;
    Node* next = head->next;
    free(head);
    destroy(next);
}

int areTasksAvailable(Node* head, int currentTime)
{
    if (head == NULL)
        return 0;
    if (head->hasRun == 0 && head->arrivalTime <= currentTime)
        return 1;
    return areTasksAvailable(head->next, currentTime);
}
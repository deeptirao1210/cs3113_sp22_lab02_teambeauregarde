#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <getopt.h>

struct node {
	int name;
	int arrivaltime;
	int runtime;
	int run;
	int turnaround;
	int response;
	_Bool finished;
	struct node* next;
};

void destroy(struct node* head);

int main(int argc, char* argv[]) {
	char* infile_name;
	char* outfile_name;
	int slice;

	if (argc != 7) {
		printf("Incorrect number of arguments provided, expected 6\n");
		exit(0);
	}
	int c;

    while (1)
    {
        int option_index = 0;
        static struct option long_options[] =
        {
            {"workload-file", required_argument, NULL, 0},
            {"result-file", required_argument, NULL, 0},
			{"timeslice", required_argument, NULL, 0},
            {NULL, 0, NULL, 0}
        };

        c = getopt_long(argc, argv, "-:", long_options, &option_index);
        if (c == -1)
            break;
        
        switch (c)
        {
            case 0:
                if (strcmp(long_options[option_index].name, "workload-file") == 0)
                    infile_name = optarg;
                if (strcmp(long_options[option_index].name, "result-file") == 0)
                    outfile_name = optarg;
				if (strcmp(long_options[option_index].name, "timeslice") == 0)
                    slice = atoi(optarg);
                break;
            default:
                printf("?? getopt returned character code 0%o ??\n", c);
        }
    }

	FILE* in_file = fopen(infile_name, "r");
	FILE* out_file = fopen(outfile_name, "w");
	if (in_file == NULL) {
		printf("Error opening workload file\n");
		if (out_file != NULL)
			fclose(out_file);
		exit(0);
	}
	if (out_file == NULL) {
		printf("Error opening result file\n");
		fclose(in_file);
		exit(0);
	}
	
	if (slice <= 0) {
		printf("Time slice must be a positive integer\n");
		fclose(in_file);
		fclose(out_file);
		exit(0);
	}

	// Build linked list
	char input[20] = {0,};
	fgets(input, 20, in_file);
	struct node* list = NULL;
	struct node* tail = NULL;
	while (input[0] != 0) {
		// Construct new node
		struct node* temp = (struct node*)malloc(sizeof(struct node));
		temp->name = atoi(strtok(input, ","));
		temp->arrivaltime = atoi(strtok(NULL, ","));
		temp->runtime = atoi(strtok(NULL, ","));
		temp->run = temp->runtime;
		temp->turnaround = 0;
		temp->response = -1;
		temp->finished = false;
		temp->next = NULL;

		// Add to list
		if (list == NULL) {
			list = temp;
			tail = temp;
		}
		else {
			tail->next = temp;
			tail = temp;
		}
		if (fgets(input, 20, in_file) == NULL) {
			break;
		}

		tail->next = temp;
		tail = temp;
	}

	// Schedule
	struct node* temp = list;
	int turnaround = 0;
	_Bool finished = false;
	_Bool pass = true;
	while (finished == false) {
		finished = true;
		pass = true;		
		while (temp != NULL) {
			if (temp->finished == true) {
				temp = temp->next;
				continue;
			}
			finished = false;
			
			if (temp->arrivaltime > turnaround) {
				temp = temp->next;
				continue;
			}

			if (temp->response < 0) {
				temp->response = turnaround - temp->arrivaltime;
			}

			pass = false;
			turnaround += slice;
			temp->run -= slice;
			if (temp->run <= 0) {
				temp->finished = true;
				temp->turnaround = turnaround + temp->run;
				turnaround += temp->run;
			}

			temp = temp->next;
		}
		temp = list;
		if (pass) {
			turnaround += slice;
		}
	}

	temp = list;
	while (temp != NULL) {
		fprintf(out_file, "%i,%i,%i,%i,%i\n", temp->name, temp->arrivaltime, temp->runtime, temp->turnaround, temp->response);
		temp = temp->next;
	}

	fclose(in_file);
	fclose(out_file);
	destroy(list);

	return 0;
}

void destroy(struct node* head)
{
	struct node* next = head->next;
	free(head);
	if (next == NULL)
		return;
	destroy(next);
}
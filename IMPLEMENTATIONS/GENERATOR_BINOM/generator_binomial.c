#include<stdio.h>
#include<stdlib.h>


int binomial(double p, int n);
double rand_val(int seed);

void main(int argc, char* argv[])
{

	FILE* fp;
	char* filename;
	char temp[256];
	int num_trials_arrival;
	int num_trials_runtime;
	double probability_arrival;
	double probability_runtime;
	int binomial_random_arrival[1000];
	int binomial_random_runtime[1000];
	int values_to_generate;
	int counter;

	printf("\n Program to generate Binomial Random Variables\n");
//	printf("\n Enter the output file name: ");

	//for(int i = 0; i < argc ; i++)
	//{

	if (argc != 15)
	{
		printf("wrong number of arguments, expected 15, got %i\n", argc);
		exit(-1);
	}
		
	filename = argv[2];
	fp = fopen(filename,"w");

	printf("\n Seed number:");
	printf("%s", argv[6]);
	rand_val((int)atoi(argv[6]));

	printf("\n The arrival time's probability of success:");
	printf("%s",argv[8]);
	probability_arrival = atof(argv[8]);

	printf("\n The number of trials for arrival time:");
	printf("%s",argv[10]);
	num_trials_arrival = atoi(argv[10]);

	printf("\n The  runtime's probability of success: %s", argv[12]);
	probability_runtime = atof(argv[12]);

	printf("\n The runtimes's number of trials: %s", argv[14]);
	num_trials_runtime = atoi(argv[14]);

	printf("\n  The number of values to generate:%s",argv[4]);
	//scanf("%s", temp);
	values_to_generate = atoi(argv[4]);

	printf("\n-----Generating values to file-----\n");

	for(counter = 0; counter < values_to_generate; counter++)
	{
		binomial_random_arrival[counter] =  binomial(probability_arrival, num_trials_arrival);
	
		binomial_random_runtime[counter] = binomial(probability_runtime,num_trials_runtime);

	}

	for(counter = 0; counter < values_to_generate; counter++)
	{	
		fprintf(fp,"%d,%d,%d\n",counter+1, binomial_random_arrival[counter], binomial_random_runtime[counter]);
	}

	printf("\n ------Done------");
	fclose(fp);

}

double rand_val(int seed)
{
	const long a =16807;  		//multiplier
	const long m =2147483647;       //modulus
	const long q = 127773;		//m div a
	const long r = 2836; 		// m mod a
	static long x;
	long x_div_q;
	long x_mod_q;
	long x_new;

	if(seed >0)
	{
		x = seed;
		return (0.0);
	}
	x_div_q = x/q;
	x_mod_q = x%q;
	x_new = (a * x_mod_q)- (r* x_div_q);

	if(x_new >0)
		x = x_new;
	else
		x = x_new + m;

	return( (double) x/m);

}


int binomial(double p, int n)
{
	int bin_value;
	int i;
	bin_value = 0;
	for(i=0; i < n ; i++)
	{
		if(rand_val(0) < p)
			bin_value++;
	}
	return bin_value;
}

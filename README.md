# CS 3113 Lab 02 Team Beauregarde

This is our repository for project 2. The group was Sam Bird, Ruyu Liu, and Deepti Rao.

## Tentative Plan

The problem for this lab is to implement 3 generators and 4 schedulers. Generators will generate random values for some number of jobs based on one of three distributions: binomial, uniform, and poisson. The schedulers will use the generated file and create a linked list containing the inputs. We will attempt to implement the generators by building on the files given in the pdf, and implement the schedulers by first separating the code into functions or blocks to build the linked list and implement the scheduling logic for each. 

## Statement of Work

Each group member will implement one of the generators and schedulers, except for FCFS. FCFS will be implemented together as a group to ensure everyone understands how the general implementation of a scheduler works. However, this may change depending on each team member’s availability and efficiency with consideration for the remaining time. The readme, report, and presentation will be written together as a team. 

## Schedule/timeline

The team will work on the generators and schedulers individually, and meet for implementing FCFS, writing, and presenting. The team will mostly meet remotely, and meet in person to work on presentation. The times of the meeting will depend on the schedules of each member and the soonest available time.

## Meeting notes
There was disagreement on the calculation of response time for schedulers, which was resolved by reviewing the textbook and each other’s implementations. We disagreed on the definition of response time and which of the implemented variables would result in the correct response for various schedulers.


## Self-assessment

Deepti Rao: 33.33%
Ruyu Liu: 33.33%
Sam Bird: 33.33%

## Reflections

If the lab could be done again, the team would attempt to schedule more meetings to discuss the report responses, whether remotely or in-person. 
